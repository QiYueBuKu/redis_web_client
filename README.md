## redis admin
Redis Admin是一个Redis管理平台，主要用于方便查看Key信息。

目前支持`单机Redis`和`Redis Cluster`模式

如果您有好的建议或需求欢迎私信

## 相关文档
**[WIKI](https://gitee.com/careyjike_173/redis_web_client/wikis/Home)**

## 问题反馈
通过 **[Issues](https://gitee.com/careyjike_173/redis_web_client/issues)** 进行反馈

## 截图

![](https://gitee.com/careyjike_173/redis_web_client/raw/master/static/img/1.png)


![](https://gitee.com/careyjike_173/redis_web_client/raw/master/static/img/2.png)

![](https://gitee.com/careyjike_173/redis_web_client/raw/master/static/img/3.png)